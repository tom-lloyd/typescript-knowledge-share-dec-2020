import middy from '@middy/core';
import { Context } from 'aws-lambda';

/**
 *  1. Define a middleware function to decorate a lambda handler.
 *     (Can use 'before', 'after' or 'onError' events)
 */
const errorHandler = (): middy.MiddlewareObject<any, any> => ({
    onError: async (handler: middy.HandlerLambda): Promise<void> => {
        handler.response = {
            statusCode: 500,
            message: 'Unknown error'
        };
    }
});

/**
 *  2. Wrap the lambda handler with 'middy()' and add middleware with '.use()'.
 */
const handler = middy(async (event: any): Promise<void> => {
    throw new Error();
})
.use(errorHandler());

test('Catch global error and return generic response', async () => {
    await expect(invoke(handler)).resolves.toEqual({
        statusCode: 500,
        message: 'Unknown error'
    });
});

function invoke(handler: middy.Middy<any, any, Context>): Promise<any> {
    return new Promise((resolve) => {
        handler({}, {} as Context, (_, response) => resolve(response));
    })
}
