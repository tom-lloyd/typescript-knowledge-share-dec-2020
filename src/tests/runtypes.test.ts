import { Record, Static, String } from 'runtypes';

/**
 *  1. Define model with types from 'runtypes' library.
 *     This will be used to validate an object conforms to a type and return that type. 
 */
export const DbConfig = Record({
    host: String,
    user: String,
    password: String
});

/**
 *  2. Retrieve the static type (same as declaring the type normally).
 *     (Notice variable above and this type have same name- this is still valid).
 */
export type DbConfig = Static<typeof DbConfig>;

test('Validate and retrieve type for valid object', () => {
    const expected = {
        host: 'host',
        user: 'user',
        password: 'password'
    };

    const test = expected as any;

    expect(DbConfig.check(test)).toEqual<DbConfig>(expected);
});

test('Throw error for type mismatch', () => {
    const test = {
        host: 1,
        user: 2,
        password: 3
    };

    expect(() => DbConfig.check(test)).toThrow();
});

test('Throw error for missing property', () => {
    const test = {
        host: 'host',
        user: 'user'
        // no password
    };

    expect(() => DbConfig.check(test)).toThrow();
});
